# Task

Add this functionality to reservations' component:

User should be able to see actual reservation on calendar (marked with grey). 

User should by able to dynamicly select from and to dates on calendar (mark selection with orange).

User should be able to add reservation to the list of existing.

Use reservations service to get/add reservations. 

Use interactive desing: [link](https://xd.adobe.com/view/609f3fdb-525e-4fbb-4c94-6f9954c99983-16ae/?fbclid=IwAR3anDb8jgxzuICOUJiUWDazCQatiq8yPlExJL8mBsZ-YBCaQGVoeEm4ij4)

Please, use prettier as a formatting tool. 

Please, fork a repository to send us back your results.