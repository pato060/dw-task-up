import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Reservation } from '../common/declarations';
import * as moment from 'moment/moment';

@Injectable({
    providedIn: 'root',
})
export class ReservationsService {
    private reservations$ = new BehaviorSubject<Reservation[]>(this.createMock());
    private createMock(): Reservation[] {
        return [
            {
                from: moment().add(-5, 'd').startOf('d'),
                to: moment().add(-2, 'd').endOf('d'),
                name: 'Ujo Mrkvicka',
                personsCount: 1,
            },
            {
                from: moment().add(2, 'd').startOf('d'),
                to: moment().add(5, 'd').endOf('d'),
                name: 'Jozo Sedy',
                personsCount: 2,
            },
        ];
    }

    async addReservation(reservation: Reservation) {
        await new Promise((resolve) => setTimeout(resolve, 600));
        const reservations = [...this.reservations$.value];
        if (
            reservations.find((x) => moment(x.from).isBefore(reservation.to) && moment(reservation.from).isBefore(x.to))
        ) {
            throw new Error('Date intervals overlap!');
        }
        reservations.push(reservation);
        this.reservations$.next(reservations);
    }

    getReservations() {
        return this.reservations$;
    }
}
