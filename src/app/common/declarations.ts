import { Moment } from 'moment/moment';

export interface Reservation {
    from: Moment;
    to: Moment;
    name: string;
    personsCount: number;
}
