import { Component, OnInit } from '@angular/core';
import { ReservationsService } from '../services/reservations.service';
import { Reservation } from '../common/declarations';
import { Moment } from 'moment';
import * as moment from 'moment';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-reservations',
    templateUrl: './reservations.component.html',
    styleUrls: ['./reservations.component.scss'],
})
export class ReservationsComponent implements OnInit {
    value: number = 1;
    step: number = 1;
    min: number = 1;
    max: number = 10;
    wrap: boolean = false;
    addedToBasket: boolean = false;
    reservations: Reservation[];
    formGroup: FormGroup;

    constructor(private reservationsService: ReservationsService, private _snackBar: MatSnackBar) {}

    ngOnInit() {
        this.reservations = this.reservationsService.getReservations().value;
        this.reservationsService.getReservations().subscribe(this.logReservations);

        const controls = {
            nameAndSurname: new FormControl(''),
            startDate: new FormControl(''),
            endDate: new FormControl(''),
            countOfPerson: new FormControl(this.value),
        };

        const nameAndSurnameValidator: ValidatorFn = (control: FormControl) => {
            if (control.value === null) return;
            if (
                control.value.includes(' ') &&
                /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð -]+$/.test(
                    control.value,
                )
            )
                if (control.value.match(/\s/g).length > 1)
                    return {
                        nameLengthError: true,
                    };
                else return null;
            else
                return {
                    nameError: true,
                };
        };

        const dateCollisionValidator: ValidatorFn = () => {
            let isReserved = false;
            this.reservations.forEach((value) => {
                if (
                    moment(controls.startDate.value).isBefore(value.from) &&
                    moment(controls.endDate.value).isAfter(value.to)
                )
                    isReserved = true;
            });
            if (isReserved) return { dateCollisionError: true };
            else return null;
        };

        controls.nameAndSurname.setValidators([Validators.required, nameAndSurnameValidator]);
        controls.startDate.setValidators([Validators.required, dateCollisionValidator]);
        controls.endDate.setValidators([Validators.required, dateCollisionValidator]);

        this.formGroup = new FormGroup(controls);
    }

    logReservations(reservations: Reservation[]) {
        console.log(
            reservations.map(({ from, to, name, personsCount }) => ({
                name,
                from: moment(from).format('DD.MM.YYYY'),
                to: moment(to).format('DD.MM.YYYY'),
                personsCount,
            })),
        );
    }

    myFilter = (d: Moment | null): boolean => {
        let isReserved = false;
        this.reservations.forEach((value) => {
            if (moment(d).startOf('day').toLocaleString() === moment(value.from).startOf('day').toLocaleString())
                isReserved = true;
            else if (moment(d).startOf('day').toLocaleString() === moment(value.to).startOf('day').toLocaleString())
                isReserved = true;
            else if (moment(d).isAfter(value.from) && moment(d).isBefore(value.to)) isReserved = true;
        });
        if (isReserved) return false;
        else return true;
    };

    hasError(controlName: string, errorCode: string, controlName2?: string): boolean {
        const control = this.formGroup.controls[controlName];
        if (controlName2) {
            const control2 = this.formGroup.controls[controlName2];
            return control && control2 && (control.hasError(errorCode) || control2.hasError(errorCode));
        } else return control && control.hasError(errorCode);
    }

    openSnackBar(message: string, action: string) {
        this._snackBar.open(message, action, {
            duration: 6000,
        });
    }

    submitForm() {
        if (this.value > 0 && !this.addedToBasket) this.addedToBasket = true;
        let reservation: Reservation = {
            from: this.formGroup.controls['startDate'].value,
            to: this.formGroup.controls['endDate'].value,
            name: this.formGroup.controls['nameAndSurname'].value,
            personsCount: this.formGroup.controls['countOfPerson'].value,
        };
        this.reservationsService.addReservation(reservation).then(
            () => {
                this.addedToBasket = false;
                if (this.formGroup.valid) {
                    this.reservations = this.reservationsService.getReservations().value;
                    this.formGroup.reset();
                    this.formGroup.markAsPristine();
                    this.formGroup.markAsUntouched();
                    this.formGroup.controls['countOfPerson'].setValue(this.value);
                    this.openSnackBar('Objednávka bola pridaná!', 'Zrušiť');
                }
            },
            () => {
                this.openSnackBar('Niečo sa pokazilo..', 'Zrušiť');
            },
        );
    }
}
